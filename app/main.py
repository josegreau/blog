#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
import markdown
import os

environment = Environment(loader=FileSystemLoader(os.getcwd()))
post_list = []

def fileConvertion(folder, filename):

    template = environment.get_template(os.path.join('template',folder+'_tmpl.html.jinja'))
    if folder == 'index':
        folder = ''
    with open(os.path.join("content",folder,filename), "r") as f:
        content = f.read()
    md = markdown.Markdown(extensions = ['meta'],output_format='html5')
    content_html = md.convert(content)
    output = template.render(content=content_html, meta=md.Meta )
    if folder == 'post':
        post_list.append(md.Meta)
    if folder:
        if not os.path.exists(os.path.join("public",folder)):
            os.makedirs(os.path.join("public",folder))
    
    with open(os.path.join("public",folder,filename.replace('.md','')+".html"), "w") as fh:
        fh.write(output)


def htmlGenerator():
    print("Generating HTML files")
    
    for folder in os.listdir('content/'):

        if os.path.isdir(os.path.join('content',folder)):
            for filename in os.listdir(os.path.join('content',folder)):
                fileConvertion(folder, filename)
        else:
            fileConvertion('index', folder)

    template = environment.get_template(os.path.join('template','blog_tmpl.html.jinja'))
    output = template.render(content=post_list)
    with open(os.path.join("public","post","index.html"), "w") as fh:
        fh.write(output)

def copyStaticFiles():
	print("Copying static files")
	if not os.path.exists('public'):
		os.makedirs('public')
	if not os.path.exists('public/assets'):
		os.makedirs('public/assets')
	os.system('cp -r static/* public/assets/')  

				
if __name__ == "__main__":
    htmlGenerator()
    copyStaticFiles()	
