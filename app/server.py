# Python 3 server example
from http.server import BaseHTTPRequestHandler, HTTPServer
import time

hostName = "localhost"
serverPort = 8080
base_path = '../public'

class MyServer(BaseHTTPRequestHandler):

    def do_GET(self):
        self.path = self.path.replace('blog/', '')
        if self.path == '/':
            self.path = base_path+'/index.html'
        else:
            page = ''
            if not '.html' in self.path:
                page = '/index.html'
            self.path = base_path+self.path+ page
        try:
            #Reading the file
            file_to_open = open(self.path[1:]).read()
            self.send_response(200)
        except:
            file_to_open = "File not found"
            self.send_response(404)
        
        self.end_headers()
        self.wfile.write(bytes(file_to_open, 'utf-8'))
 

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")