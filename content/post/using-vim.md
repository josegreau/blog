Title:  Using neoVim as my editor
Summary: Using neoVim as my editor 
Authors: Jose Greau 
Date:    January 20, 2024
slug: using-vim

## First steps

Trying to use a new editor is usually frustrating. The lack of knowledge about the shortcuts, the tools, and even the "modes" of an editor results in a low eficiently work.
In fact, I've been using neovim a couple of weeks now, but I can't get used to it: Where are the tabs? How do I create a new file? How can I search a word along my files?

There are easy ways to learn how to use vim. One of them is using the VS Code plugin Extension "vim", where you can use the key combinations like vim, but with the VS CODE interface, but you are familiar with the interface and suddenly, you go back and disable the extension.
There are also some games to learn how to move, and how to use the vim modes, but it's far away than the regular use of vim. It's not only how to move, but to know when and where.

So, I thought that the best way to learn how to use vim properly, is coding, and writing, and using it every day, without usin my VS Code editor as long as I can resist. So here I'm on my first day without VS Code.

Wish me luck.

