Title:  Creating a static website on Gitlab Pages 
Summary: An easy way to create your own blog on Gitlab Pages 
Authors: Jose Greau 
Date:    April 23, 2022
slug: static-webpage

## Intro

As I like to create websites and to automate things, I tought it will be funny to create my own "website compiler". As I read, there was plenty of applications wich do that [](https://gitlab.com/pages), but at this time, my stubbornness is greater. Altought, I have already written a static web compiler for another website, and I wanted to take advantage of it, so I took the code, changed a few lines, and it was ready. The code is written in Python, because is the language that I have more experience, and maybe it could be done easily with another language, but who cares.

Generaly, I don't like fancy stuffs. I prefer more simplistic ways to do things, and in my case, I wanted to use MarkDown to write my blog. Also, I'm trying to learn a keyboard distribution called Colemak, and using my new keyboard (Corne keyboard), and finally, english is not my common language, so as you can guess, I'm writting this at 10 words per minute, wich is really frustrating.

Nevertheless, let's go with the code.

## The Python Script

The way I'm trying to create this blog, is writting everything in MD format, and then, translate it automatically to html using python and the libraries "jinja2" and "markdown" using the following script:

    for folder in os.listdir('content/'):
        post_list = []
        template = environment.get_template(os.path.join('template',folder+'_tmpl.html.jinja'))
        for filename in os.listdir(os.path.join('content',folder)):
            with open(os.path.join("content",folder,filename), "r") as f:
                content = f.read()
            md = markdown.Markdown(extensions = ['meta'],output_format='html5')
            content_html = md.convert(content)
            output = template.render(content=content_html, meta=md.Meta )
            if folder == 'post':
                post_list.append(md.Meta)

            with open(os.path.join("public",folder,filename.replace('.md','')+".html"), "w") as fh:
                fh.write(output)

    template = environment.get_template(os.path.join('template","blog_tmpl.html.jinja'))
    output = template.render(content=post_list)
    with open(os.path.join("public","post","index.html"), "w") as fh:
        fh.write(output)

	template = environment.get_template('template/blog_tmpl.html.jinja')
	output = template.render(content=post_list)
	with open("public/post/index.html", "w") as fh:
		fh.write(output)

As I said before, maybe there is a better way to do this, but this is the simplest way I found.

At first, the code will iterate over the folders in the "content" folder it could be a "page" content or a "post".
Then, we set which template to use (in this case, I'm using the same template for all my pages depending if is a normal page or a post.
Next, for each file in the folder, the markdown library will transform the md content to html content, and will use the metadata (the first lines in the md file) to create the links and set the info for each page.
Finally, the script will add the new file to the posts/page list.

I'm not dealing with images yet. Just tryingto to do a blog the simplest way I could.

The code can be found [here](https://gitlab.com/gramastudio/blog)

## The Content

As you can see in the repository, there is 5 folders:

- app: where the python script is
- content: where the md files are.
- section: where the footer and the navbar are separated from the template
- template: where the template is located
- public: where the files are ready to be published

So basically, all you have to do to use this, is create a new file in the content section, and then, run the python script (is recommended to use a virtual environment to install the libraries):

    pip install -r requirementes.txt
    python app/main.py


## Static pages on gitlab

You can use any tutorial to use your repository to host a static page, but I will leave a link so you don't have to search:[link](https://gitlab.deterrencedispensed.com/deterrence-dispensed/information-and-tutorials/-/wikis/For-Developers/Guides/How-to-create-a-static-webpage-with-GitLab-Pages)

## Last step

Finally you are ready to push your project to gitlab/github and watch your free-hosted static page.

Enjoy!
